extends Control


signal open_menu
signal open_level

func _ready() -> void:
	pass


func _input(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		if $LevelsContainer/BtnLVL1.pressed:
			emit_signal("open_level", 1)
		if $LevelsContainer/BtnLVL2.pressed:
			emit_signal("open_level", 2)
		if $LevelsContainer/BtnLVL3.pressed:
			emit_signal("open_level", 3)
		if $Back.pressed:
			emit_signal("open_menu")
