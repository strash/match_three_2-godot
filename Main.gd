extends Node


var level = 1


func set_level(lvl) -> void:
	level = lvl
	$Level.level = lvl

func _ready() -> void:
	set_level(level)
	$Menu.show()
	$MenuLevel.hide()
	$Level.hide()

	var _open_level_scene     = $Menu.connect("open_level_scene", self, "_on_Menu_open_level_scene")
	var _open_menulevel       = $MenuLevel.connect("open_menu", self, "_on_MenuLevel_open_menu")
	var _open_level_menu      = $Menu.connect("open_level", self, "_on_MenuLevel_open_level")
	var _open_level_menulevel = $MenuLevel.connect("open_level", self, "_on_MenuLevel_open_level")
	var _close_level          = $Level.connect("close_level", self, "_on_Level_close_level")



func _on_Menu_open_level_scene() -> void:
	$Menu.hide()
	$MenuLevel.show()
	$Level.hide()

func _on_MenuLevel_open_menu() -> void:
	$Menu.show()
	$MenuLevel.hide()
	$Level.hide()

func _on_MenuLevel_open_level(lvl) -> void:
	set_level(lvl)
	$Menu.hide()
	$MenuLevel.hide()
	$Level.show()
	$Level.load_level(lvl) # загружаем в фоне первый уровень
	$Level.scan_board()

func _on_Level_close_level() -> void:
	$Menu.show()
	$MenuLevel.hide()
	$Level.hide()