extends Control


signal open_level_scene
signal open_level


func _ready() -> void:
	pass


func _input(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		if $MenuContainer/StartBtn.pressed:
			emit_signal("open_level", 1)
		if $MenuContainer/LevelBtn.pressed:
			emit_signal("open_level_scene")

