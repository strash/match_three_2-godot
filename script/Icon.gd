extends Button


var type: int # вид иконки по картинке
# координаты в паттерне
var x: int
var y: int
var is_active = false setget set_is_active, get_is_active # selected
var is_subling = false setget set_is_subling, get_is_subling # selected like subling


signal icon_selected


func _ready() -> void:
	$Select.hide()
	$AnimationPlayer.stop()


func _input(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and self.pressed:
		if get_is_active():
			set_is_active(false)
		else:
			set_is_active(true)
		emit_signal("icon_selected", self)


# setter for is_active
func set_is_active(state: bool) -> void:
	if state:
		$Select.modulate.a = 1.0
		$Select.show()
	else:
		$Select.hide()
		$Select.modulate.a = 0.0
	is_active = state

# getter for is_active
func get_is_active() -> bool:
	return is_active


# setter for is_subling
func set_is_subling(state: bool) -> void:
	if state:
		$Select.modulate.a = 0.0
		$Select.show()
		$AnimationPlayer.play("select_flickering")
	else:
		$AnimationPlayer.stop()
		$Select.hide()
		$Select.modulate.a = 1.0
	is_subling = state

# getter for is_subling
func get_is_subling() -> bool:
	return is_subling


# переместить на другую позицию на доске
func go_to(new_pos: Vector2, new_x: int, new_y: int) -> void:
	self.x = new_x
	self.y = new_y
	self.rect_position = new_pos


# удаление иконки с доски
func death() -> void:
	$Tween.interpolate_property(self, "rect_scale", Vector2(1.0, 1.0), Vector2(0.0, 0.0), 0.3)
	if not $Tween.is_active():
		$Tween.start()
	yield(get_tree().create_timer(0.3), "timeout")
	queue_free()
